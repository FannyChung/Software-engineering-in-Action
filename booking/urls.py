from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.conf.urls.static import static
from django.contrib import admin
from booking import settings

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'booking.views.home', name='home'),
    # url(r'^booking/', include('booking.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += patterns('book.index',
    url(r'^$', 'index', name='index'),
)

urlpatterns += patterns('book.sign',
    url(r'^sign_in/$', 'sign_in', name='sign_in'),
    url(r'^sign_out/$', 'sign_out', name='sign_out'),
    url(r'^sign_up/$', 'sign_up', name='sign_up'),
)

urlpatterns += patterns('book.dashboard',
    url(r'^dash_board/$', 'dash_board', name='dash_board'),
    url(r'^bookings/$', 'bookings', name='bookings'),
)

urlpatterns += patterns('book.search',
    url(r'^search/$', 'search', name='search'),
)

urlpatterns += patterns('book.hotel',
    url(r'^hotel/(\d+)', 'hotel', name='hotel'),
    url(r'^set_time/', 'set_time', name='set_time'),
)

urlpatterns += patterns('book.room',
    url(r'^room/(\d+)', 'room', name='room'),
)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)