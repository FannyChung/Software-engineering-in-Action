function edit(){
    $('.set').on('click', function(){
        field = $(this)[0].classList[2].substr(4)
        $(this).parent().prev().html('<input type="text" class="form-control" name="'+field+'" placeholder="Edit">');
        $(this).parent().append('<button class="btn-link set-save">Save</button>')
        $(this).remove();
        save();

    });

    $('.set-gender').on('click', function(){
        field = 'gender'
        $(this).parent().prev().html('<input type="checkbox" value="male" name="gender"> Male </input>　　　　<input type="checkbox" value="female" name="gender"> Female</input>');
        $(this).parent().append('<button class="btn-link set-save">Save</button>')
        $(this).remove();
        $("#Setting [type='checkbox']").on('click', function(){
            if ($(this).attr('value')== 'male' && $(this).next()[0].checked){
                $(this).next()[0].checked = false;
            }else if($(this).attr('value')== 'female' && $(this).prev()[0].checked) {
                $(this).prev()[0].checked = false;
            }
        })
        save();

    });

    $('.set-smoking-room').on('click', function(){
        field = 'smoking-room'
        $(this).parent().prev().html('<input type="checkbox" value="yes" name="smoking-room"> Yes </input>　　　　<input type="checkbox" value="no" name="smoking-room"> No</input>');
        $(this).parent().append('<button class="btn-link set-save">Save</button>')
        $(this).remove();
        $("#Setting [type='checkbox']").on('click', function(){
            if ($(this).attr('value')== 'male' && $(this).next()[0].checked){
                $(this).next()[0].checked = false;
            }else if($(this).attr('value')== 'female' && $(this).prev()[0].checked) {
                $(this).prev()[0].checked = false;
            }
        })
        save();

    });

    $('.set-password').on('click', function(){
        $(this).parent().prev().html('<input type="password" name="password" class="form-control required"> <input type="password" name="confirm-password" class="form-control required">');
        $(this).parent().append('<button class="btn-link set-save">Save</button>')
        $(this).remove();
        save();

    });

}

function save(){
    $('.set-save').on('click',function(){
        field = $(this).parent().prev().children().attr('name');
        value = $(this).parent().prev().children().val();
        if (field=='gender'){
            checkbox_list = $("#Setting [type='checkbox']");
            for(i = 0; i < checkbox_list.length; i++){
                if (checkbox_list[i].checked){
                    if (i==0){
                        value = 'Male'
                        $(this).parent().prev().html('Male');
                    }else if (i==1){
                        value = 'Female'
                        $(this).parent().prev().html('Female');
                    }
                    $(this).parent().append('<button class="btn-link set-'+field+'">Edit</button>')
                    $(this).remove();
                    edit();
                }
            }
        }
        else if (field=='smoking-room'){
            checkbox_list = $("#Setting [type='checkbox']");
            for(i = 0; i < checkbox_list.length; i++){
                if (checkbox_list[i].checked){
                    if (i==0){
                        value = 'Yes'
                        $(this).parent().prev().html('Yes');
                    }else if (i==1){
                        value = 'No'
                        $(this).parent().prev().html('No');
                    }
                    $(this).parent().append('<button class="btn-link set-'+field+'">Edit</button>')
                    $(this).remove();
                    edit();
                }
            }
        }
        else if (field=='password'){
            password = $($(this).parent().prev().children()[0]).val()
            confirm_password = $($(this).parent().prev().children()[1]).val()
            if (password.length < 7){
                $('label').remove('.error')
                $(this).parent().prev().append('<label class="error">At least seven characters</lable>')
                $($(this).parent().prev().children()[0]).val('')
                $($(this).parent().prev().children()[1]).val('')
                $($(this).parent().prev().children()[0]).focus()
            }
            else {
                if (password == confirm_password){
                    value = password
                    $(this).parent().prev().html('******')
                    $(this).parent().append('<button class="btn-link set-'+field+'">Edit</button>')
                    $(this).remove();
                    edit();
                }else{
                    $('label').remove('.error')
                    $(this).parent().prev().append('<label class="error">The confirm password is different with password</lable>')
                    $($(this).parent().prev().children()[0]).val('')
                    $($(this).parent().prev().children()[1]).val('')
                    $($(this).parent().prev().children()[0]).focus()
                }
            }
        }
        else if (value){
            $(this).parent().prev().html(value)
            $(this).parent().append('<button class="btn-link set set-'+field+'">Edit</button>')
            $(this).remove();
            edit();
        }

        if (field == 'smoking-room'){
            field = 'smoking_room'
        }else if (field == 'tel'){
            field = 'phone_number'
        }

        $.ajax({
            url: '/dash_board/',
            type: 'POST',
            csrf: csrftoken,
            data:{
                'type':'setting',
                'field':field,
                'value':value
            }
        })
    });
}

$(document).ready(function(){
    edit();
});