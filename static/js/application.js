/**
 * Created with PyCharm.
 * User: xindervella
 * Date: 10/12/13
 * Time: 9:31 PM
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function(){
//    日历
    $(function() {
        $( "#from-date" ).datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1,
            onClose: function( selectedDate ) {
                $( "#to-date" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to-date" ).datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1,
            onClose: function( selectedDate ) {
                $( "#from-date" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });

    $(function() {
        $( "#set-from-date" ).datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1,
            onClose: function( selectedDate ) {
                $( "#set-to-date" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#set-to-date" ).datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1,
            onClose: function( selectedDate ) {
                $( "#fset-from-date" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
});