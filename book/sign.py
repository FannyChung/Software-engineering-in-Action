# -*- coding:utf-8 -*-
'''

Sign in & Sign up & Sign out

author: xindervella

'''

from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_exempt
from book.models import UserDetail


def _sign_in(request, username, password):
    '''
    sign core function
    '''
    ret = False
    user = authenticate(username=username, password=password)
    if user:
        if user.is_active:
            auth_login(request, user)
            ret = True
        return ret
    else:
        return ret


def sign_in(request):
    if request.method == 'GET':
        data = {
            'url': 'sign_in',
        }
        data.update(csrf(request))
        return render_to_response('sign_in.html', data)
    if request.method == 'POST':
        try:
            username = request.POST['username']
            password = request.POST['password']
        except KeyError:
            return HttpResponse('incomplete information')
        if _sign_in(request, username, password):
            return HttpResponseRedirect('/')
        else:
            return HttpResponseRedirect('/sign_in/')

def sign_up(request):
    if request.method == 'GET':
        data = {
            'url': 'sign_up',
        }
        data.update(csrf(request))
        return render_to_response('sign_up.html', data)
    if request.method == 'POST':
        try:
            username = request.POST['username']
            password = request.POST['password']
            confirm_password = request.POST['confirm_password']
            name = request.POST['name']
            email =request.POST['email']
            tel = request.POST['tel']
            address = request.POST['address']
            city = request.POST['city']
            country = request.POST['country']
            zip_code = request.POST['zip_code']
        except KeyError:
            return HttpResponse('incomplete information')
        if password != confirm_password:
            print 'check password'
            return HttpResponse('check password')
        else:
            try:
                User.objects.get(username=username)
                return HttpResponse('user already exist')

            except ObjectDoesNotExist:
                user = User.objects.create_user(username=username, password=password)
                UserDetail.objects.create(user=user, name=name, phone_number=tel, email=email,
                                          address=address, city=city, country=country, zip_code=zip_code)
                _sign_in(request, username, password)
                return HttpResponse('success')


def sign_out(request):
    auth_logout(request)
    return HttpResponseRedirect('/')