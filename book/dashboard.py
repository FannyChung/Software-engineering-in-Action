# -*- coding:utf-8 -*-
'''

Dash Board view

author: xindervella

'''
from django.contrib.auth.models import User
from django.core.context_processors import csrf
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from book.models import UserDetail, CreditCard, BookingList, RoomBooking


def dash_board(request, channel='profile'):
    if request.method == 'POST':
        if request.user.is_authenticated():
            user = User.objects.get(username=request.user)
            try:
                type = request.POST['type']
            except KeyError:
                return HttpResponse(status=404)
            if type == 'setting':
                try:
                    field = request.POST['field']
                    value = request.POST['value']
                    if field == 'password':
                        user.set_password(value)
                        user.save()
                    else :
                        UserDetail.objects.filter(user=user).update(**{field: value})
                except KeyError:
                    return HttpResponse(status=404)
        else:
            return HttpResponse(status=404)

    if request.method == 'GET':
        if request.user.is_authenticated():
            user = User.objects.get(username=request.user)
            try:
                user_detail = UserDetail.objects.get(user=user)
            except ObjectDoesNotExist:
                user_detail = None
            try:
                credit_card = CreditCard.objects.get(user=user)
            except ObjectDoesNotExist:
                credit_card = None

            booking_lists = BookingList.objects.filter(user=user)
            booking_rooms = []
            for booking_list in booking_lists:
                booking_rooms += list(RoomBooking.objects.filter(booking_list=booking_list))



            data = {
                'url': 'profile',
                'channel':channel,
                'user': user,
                'user_detail': user_detail,
                'credit_card': credit_card,
                'booking_rooms':booking_rooms,
            }
            data.update(csrf(request))
            return render_to_response('dash_board.html', data)
        else:
            return HttpResponseRedirect('/')


def bookings(request):
    return dash_board(request, 'bookings')