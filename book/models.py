# -*- coding:utf-8 -*-
from django.db import models
from django.contrib.auth.models import User

class UserDetail(models.Model):
    '''
    用户信息
    '''
    user = models.ForeignKey(User)
    name = models.CharField(max_length=20, blank=True)
    address = models.CharField(max_length=50, blank=True)
    country = models.CharField(max_length=50, blank=True)
    city = models.CharField(max_length=50, blank=True)
    zip_code = models.CharField(max_length=20, blank=True)  # 邮编
    phone_number = models.CharField(max_length=20, blank=True)
    gender = models.CharField(max_length=5, blank=True) # 性别
    smoking_room = models.CharField(max_length=5, blank=True)
    id_card = models.CharField(max_length=18, blank=True)
    email = models.EmailField(blank=True)
    travel_times = models.IntegerField(default=0)  # 出行次数
    destination_number = models.IntegerField(default=0)  # 目的地数
    night_number = models.IntegerField(default=0)  # 入住天数
    country_number = models.IntegerField(default=0)  # 国家数

    def __unicode__(self):
        return self.name

class CreditCard(models.Model):
    '''
    信用卡
    '''
    user = models.ForeignKey(User)
    number = models.IntegerField()
    valid_thru = models.CharField(max_length=20)
    cvc2 = models.CharField(max_length=10)
    def __unicode__(self):
        return self.number

class Hotel(models.Model):
    '''
    酒店信息
    '''
    name = models.CharField(max_length=50, blank=True)
    country = models.CharField(max_length=20, blank=True)
    state = models.CharField(max_length=20, blank=True)
    city = models.CharField(max_length=20, blank=True)
    tel = models.CharField(max_length=20, blank=True)
    up = models.IntegerField(default=0)
    down = models.IntegerField(default=0)
    nearest_booking = models.DateTimeField(auto_now=True)
    info = models.TextField(blank=True)
    photo = models.ImageField(default='photos/None/no-img.jpg', blank=True, upload_to='photos/')
    def __unicode__(self):
        return self.name

class Room(models.Model):
    '''
    房间信息
    '''
    hotel = models.ForeignKey(Hotel)
    number = models.CharField(max_length=10)
    # type = models.CharField(max_length=10)
    people_limit = models.SmallIntegerField()
    price = models.IntegerField()
    def __unicode__(self):
        return '%s %s' % (self.hotel.name, self.number)

class BookingList(models.Model):
    '''
    订单信息
    '''
    user = models.ForeignKey(User)
    time = models.DateTimeField(auto_now=True)
    def __unicode__(self):
        return unicode(self.id)

class RoomBooking(models.Model):
    '''
    房间预定信息
    '''
    booking_list = models.ForeignKey(BookingList)
    room = models.ForeignKey(Room)
    from_date = models.DateField()
    to_date = models.DateField()
    def __unicode__(self):
        return unicode(self.booking_list.id)
