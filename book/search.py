# -*- coding:utf-8 -*-
"""
Search

author: xindervella
"""
import datetime
from django.contrib.auth.models import User
from django.core.context_processors import csrf
from django.core.exceptions import ObjectDoesNotExist

from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render_to_response
from book.models import UserDetail, Hotel, Room, RoomBooking


def search(request):
    from_date = ''
    to_date = ''
    request.session['from_date'] = from_date
    request.session['to_date'] = to_date
    if request.user.is_authenticated():
        user = User.objects.get(username=request.user)
        try:
            user_detail = UserDetail.objects.get(user=user)
        except ObjectDoesNotExist:
            user_detail = None
    else:
        user = None
        user_detail = None

    if request.method == 'POST':
        try:
            type = request.POST['type']
        except KeyError:
            type = None
        if type == 'quick_search':
            condition = request.POST['condition']
            hotels = Hotel.objects.filter(
                Q(name__contains=condition) | Q(country__contains=condition) | Q(state__contains=condition) |
                Q(city__contains=condition))
        elif type == 'search':
            condition = request.POST['condition']
            from_date = request.POST['from-date']
            to_date = request.POST['to-date']

            request.session['from_date'] = from_date
            request.session['to_date'] = to_date

            predefined_number = request.POST['predefined-number']
            hotels = set(Hotel.objects.filter(
                Q(name__contains=condition) | Q(country__contains=condition) | Q(state__contains=condition) |
                Q(city__contains=condition)))

            gte_from_date = []
            lte_to_date = []
            if from_date:
                try:
                    from_date = datetime.datetime.strptime(from_date, '%m/%d/%Y').date()
                    gte_from_date = list(RoomBooking.objects.filter(from_date__lte=from_date, to_date__gte=from_date))
                except ValueError:
                    gte_from_date = []
            if to_date:
                try:
                    to_date = datetime.datetime.strptime(to_date, '%m/%d/%Y').date()
                    lte_to_date = list(RoomBooking.objects.filter(from_date__lte=to_date, to_date__gte=to_date))
                except ValueError:
                    print 'to_date ValueError'
                    lte_to_date = []
            unsuitable_rooms = set([room_booking.room for room_booking in gte_from_date + lte_to_date])

            rooms = []
            if predefined_number:
                try:
                    predefined_number = int(predefined_number[0])
                    rooms = Room.objects.filter(people_limit=predefined_number)
                except ValueError:
                    # print 'predefined_number ValueError'
                    rooms = []
            suitable_rooms = set(rooms) - unsuitable_rooms
            # print suitable_rooms
            suitable_hotels = set([room.hotel for room in rooms])
            # print suitable_hotels
            hotels = hotels & suitable_hotels
        else:
            return HttpResponse(status=404)

        search_result = {}
        for hotel in hotels:
            rooms = Room.objects.filter(hotel=hotel).order_by('people_limit')
            search_result[hotel] = [room for room in rooms]


        data = {
            'url': 'search',
            'user': user,
            'user_detail': user_detail,
            'hotels': hotels,
            'search_result': search_result,
        }
        data.update(csrf(request))
        return render_to_response('search.html', data)