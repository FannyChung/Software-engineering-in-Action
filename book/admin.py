from django.contrib import admin
from book.models import UserDetail, CreditCard, Hotel, Room, BookingList, RoomBooking

class UserDetailAdmin(admin.ModelAdmin):
    list_display = ('user', 'name', 'gender', 'city', 'country', 'phone_number', 'email', 'smoking_room')
    search_fields = ('user__username', 'name', 'phone_number', 'email', 'city', 'country')
    list_filter = ('gender', 'smoking_room')

admin.site.register(UserDetail, UserDetailAdmin)


class CreditCardAdmin(admin.ModelAdmin):
    list_display = ('user', 'number', 'valid_thru', 'cvc2')
    search_fields = ('user', 'number')

admin.site.register(CreditCard, CreditCardAdmin)


class HotelAdmin(admin.ModelAdmin):
    list_display = ('name', 'country', 'state', 'city', 'tel', 'up', 'down', 'nearest_booking')
    search_fields = ('name', 'country', 'state', 'city')

admin.site.register(Hotel, HotelAdmin)


class RoomAdmin(admin.ModelAdmin):
    list_display = ('hotel', 'number', 'people_limit', 'price')
    search_fields = ('hotel_name', 'people_limit')

admin.site.register(Room, RoomAdmin)


class BookingListAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'time')
    search_fields = ('user__username', 'hotel')

admin.site.register(BookingList, BookingListAdmin)

class RoomBookingAdmin(admin.ModelAdmin):
    list_display = ('booking_list', 'room', 'from_date', 'to_date')

admin.site.register(RoomBooking, RoomBookingAdmin)