# -*- coding:utf-8 -*-
"""
Index view
author: xindervella
"""

import random
from django.contrib.auth.models import User
from django.core.context_processors import csrf
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse
from django.shortcuts import render_to_response
from book.models import UserDetail, Room, Hotel


def index(request):
    if request.method == 'GET':
        if request.user.is_authenticated():
            user = User.objects.get(username=request.user)
            try:
                user_detail = UserDetail.objects.get(user=user)
            except ObjectDoesNotExist:
                user_detail = None
        else:
            user = None
            user_detail = None

        index_dict = {}
        hotel_set = Hotel.objects.all()
        hotel_number = {}
        cities = list(set([hotel.city for hotel in hotel_set]))
        cities = [cities[i] for i in random.sample(xrange(len(cities)), 2)]

        for city in cities:
            hotels = {}
            for hotel in Hotel.objects.filter(city=city).order_by('up')[:6]:
                try:
                    room_1 = [Room.objects.filter(hotel=hotel).order_by('price')[0]]
                except IndexError:
                    room_1 = []
                try:
                    room_2 = [Room.objects.filter(hotel=hotel, people_limit=1)[0]]
                except IndexError:
                    room_2 = []
                try:
                    room_3 = [Room.objects.filter(hotel=hotel, people_limit=2)[0]]
                except IndexError:
                    room_3 = []
                hotels[hotel] = room_1 + room_2 + room_3
            # print city
            index_dict[city] = hotels
            # print index_dict
            hotel_number[city] = len(Hotel.objects.filter(city=city))

        data = {
            'url': 'index',
            'user': user,
            'user_detail': user_detail,
            'index_dict': index_dict,
            'hotel_number': hotel_number,
        }
        data.update(csrf(request))
        return render_to_response('index.html', data)
    else:
        return HttpResponse(status=404)