# -*- coding:utf-8 -*-
"""
Room Booking view
author: xindervella
"""

import datetime
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404, HttpResponse, HttpResponseRedirect
from book.models import Room, RoomBooking, BookingList
from django.contrib.auth.models import User

def room(request, room_id):
    if request.user.is_authenticated():
        user = User.objects.get(username=request.user)
    else:
        return HttpResponse('/sign_in/')

    from_date = request.session['from_date']
    to_date = request.session['to_date']

    from_date = datetime.datetime.strptime(from_date, '%m/%d/%Y').date()
    to_date = datetime.datetime.strptime(to_date, '%m/%d/%Y').date()
    try:
        room = Room.objects.get(id=room_id)
    except ObjectDoesNotExist:
        return Http404
    booking_list = BookingList.objects.create(user=user)
    RoomBooking.objects.create(booking_list=booking_list, room=room, from_date=from_date, to_date=to_date)

    return HttpResponseRedirect('/bookings/')