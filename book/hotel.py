# -*- coding:utf-8 -*-
"""
Hotel view
author: xindervella
"""
import datetime
from django.core.context_processors import csrf
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404, HttpResponse
from django.shortcuts import render_to_response
from book.models import Hotel, Room, RoomBooking, UserDetail
from django.contrib.auth.models import User

def hotel(request, hotel_id):
    if request.user.is_authenticated():
        user = User.objects.get(username=request.user)
        try:
            user_detail = UserDetail.objects.get(user=user)
        except ObjectDoesNotExist:
            user_detail = None
    else:
        user = None
        user_detail = None

    if request.method == 'GET':
        try:
            hotel_id = int(hotel_id)
        except ValueError:
            raise Http404()
        try:
            hotel = Hotel.objects.get(id=hotel_id)
        except ObjectDoesNotExist:
            raise Http404()
        try:
            from_date = request.session['from_date']
            to_date = request.session['to_date']
        except KeyError:
            from_date = ''
            to_date = ''

        if not from_date and not to_date:
            is_set_tiem = False
        else:
            is_set_tiem = True

        if from_date:
            try:
                from_date = datetime.datetime.strptime(from_date, '%m/%d/%Y').date()
                gte_from_date = list(RoomBooking.objects.filter(from_date__lte=from_date, to_date__gte=from_date))
            except ValueError:
                gte_from_date = []
        else:
            gte_from_date = []
        if to_date:
            try:
                to_date = datetime.datetime.strptime(to_date, '%m/%d/%Y').date()
                lte_to_date = list(RoomBooking.objects.filter(from_date__lte=to_date, to_date__gte=to_date))
            except ValueError:
                lte_to_date = []
        else:
            lte_to_date = []
        temp_room_bookings = gte_from_date + lte_to_date
        unsuitable_rooms = set([room_book.room for room_book in temp_room_bookings])
        rooms = list(set(Room.objects.filter(hotel=hotel)) - unsuitable_rooms)

        data = {
            'url': 'hotel',
            'user': user,
            'user_detail': user_detail,
            'rooms': rooms,
            'hotel': hotel,
            'is_set_time': is_set_tiem,
        }
        data.update(csrf(request))

        return render_to_response('hotel.html', data)


def set_time(request):
    if request.method == 'POST':
        try:
            from_date = request.POST['from_date']
            to_date = request.POST['to_date']
        except KeyError:
            return HttpResponse('incomplete information')

        request.session['from_date'] = from_date
        request.session['to_date'] = to_date

        return HttpResponse('success')